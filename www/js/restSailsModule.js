angular.module('restSailsModule', [])
.factory('RestSails', function($http){
  return {
          getAllUsers : function(){
            return $http.get('http://localhost:1337/user');
          },
          getAllPets : function(){
            return $http.get('http://localhost:1337/pet');
          },
          createUser : function(user){
            return $http.post('http://localhost:1337/user/', user);
          },
          createPet : function(pet){
            return $http.post('http://localhost:1337/pet/', pet);
          }
   };
})